Scrapy Spider Example
===

Description of spider.py
---

This is the part of a high load crawling system, that should index a lot of pages on a
different websites recursively until some `settings.DEPTH_LIMIT`.
 
Project was built on scrapy-redis and django frameworks. From django we have used Django ORM to
easily index managing. Scrapy-Redis as the main scraping framework.

Each indexed page should be processed by universal content scrapper with approximately
matching content sections and results will be stored to another database.

**spider.py** used following solutions:

 - BeautifulSoup html parser library
 - Scrapy RedisSpider
 - Custom RegExpLinkExtractor as it's faster than any other solutions.
 - Custom offsite request filter. Was needed as it's universal spider and it's should crawl only child 
 pages of provided website.
