#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import math
import datetime
from crawler import settings
from BeautifulSoup import BeautifulSoup
from scrapy_redis.spiders import RedisSpider
from tool import debug, models, crawler
from scrapy.http import Request
from crawler.extractor import RegexLinkExtractor
from urlparse import urlparse


class SqlBufferedCrawler(RedisSpider, crawler.CrawlerTool):
    # Crawler name
    name = 'sqlbuff'

    # Link extractor
    extractor = RegexLinkExtractor(
        deny=(
            r'print',
            r'brochure',
            r'://m.',
            r'://mobi.'
        )
    )

    # RAM Agencies cache
    agencies_cache = {}

    # RAM Agencies related ids cache
    related_ids = {}

    # RAM Cached agencies allowed domains
    allowed_domains = {}

    # Stored pages counter for limit
    stored_count = {}

    def __init__(self, name=None, **kwargs):
        super(SqlBufferedCrawler, self).__init__(name, **kwargs)
        self.items = []

    def process_response(self, response):
        """
        Process response and schedule item for saving
        :param response:
        :return: item
        """
        agency = self._agency(response.meta['item'].agency_id)
        if agency.parent_id:
            agency = self._agency(agency.parent_id)

        content = self._get_json_content(response)

        if content is not None:
            self.items.append(models.PageIndex(
                url=response.url,
                agency=agency,
                parent=response.request.headers.get('Referer', agency.website),
                content=content
            ))

        if len(self.items) >= settings.PAGECACHE_CHUNK_SIZE:
            # Mark current agency
            self.server.set('crawler:agency', agency.website)

            # Save pages to database
            created = self._insert_cache(self.items)
            self.items = []
            self.logger.debug(debug.success('Created {} pages'.format(created)))

    def parse(self, response):
        """
        Parse links from response and schedule valid
        :param response:
        :return:
        """
        # Sitemap object
        page = response.meta['item']

        # Get current agency
        agency = self._agency(page.agency_id)

        # Current agency depth limit
        depth_limit = agency.depth_level or settings.DEPTH_LIMIT

        # Allowed domains
        allowed_domains = agency.allowed_domains

        # List of new objects
        schedule_objects = []

        if page.is_sitemap:
            self.logger.debug(
                debug.warning('Allowed domains {}'.format(allowed_domains))
            )

            # Detect sitemap response and schedule urls to parse
            sitemap = BeautifulSoup(self._get_sitemap_body(response))
            for loc in sitemap.findAll('loc'):
                if loc.parent.name in 'sitemap' or self._is_sitemap(loc.text):
                        try:
                            models.SiteMap(
                                agency=agency,
                                url=loc.text,
                                is_sitemap=True,
                                parent=page
                            ).save()
                            self.logger.debug(debug.success('Scheduled child sitemap: {}'.format(loc.text)))
                        except Exception as e:
                            self.logger.debug(debug.warning('Can\'t schedule sitemap: {}'.format(e)))

                # Schedule page parse
                else:
                    schedule_objects.append(
                        page.create_child(loc.text)
                    )
        else:
            # Extract links from parsed page
            # Schedule valid internal URLs
            if depth_limit > page.depth_level > -1 and self._allowed_extract(agency):
                self.logger.debug(
                    debug.info('Extracting links from page {}'.format(response.url))
                )
                time = self._timedelta()
                links = self.extractor.extract_response(response)
                time = self._timedelta(time) * 1000
                self.logger.debug(
                    debug.info('Extracted {} links in {} mSec.'.format(len(links), time))
                )

                for url in links:
                    if not url.startswith('http'):
                        url = '{}://{}'.format(urlparse(response.url).scheme, url)

                    # Filter offsite requests
                    if self._is_offsite(allowed_domains, url):
                        self.logger.debug(debug.fail('Offsite request filtered: {}'.format(url)))
                        continue

                    schedule_objects.append(
                        page.create_child(url)
                    )

            # Process page response
            yield self.process_response(response)

        # Schedule next requests to database
        added = self._insert_sitemap(schedule_objects, agency)
        self.logger.debug(debug.success('Scheduled {} pages of {} links'.format(added, len(schedule_objects))))

    def recrawl_next(self):
        """
        Automatic scheduling
        :return:
        """
        if not settings.AUTO_SCHEDULING:
            return

        # Get agencies ids with listings that has no matches
        agencies = models.Listing.objects.exclude(
            id__in=models.ExternalListing.objects.values_list('listing_id', flat=True).distinct()
        ).values_list('agency_id', flat=True).distinct().all()

        # Search for agencies that has not matched listings
        agencies = models.Agency.objects.filter(
            disabled=False,
            alias=False,
            parent=None,
            id__in=list(agencies)
        ).order_by('updated_at').all()

        # Schedule agencies
        for agency in agencies[:settings.AUTO_SCHEDULING_SIZE]:
            # Schedule caching
            agency.update()
            self.logger.debug(debug.success('Scheduled crawling {}'.format(agency.website)))

    def schedule_next_request(self):
        """
        Schedule next request or get new chunk from MySQL DB
        Then save stored pages to database
        :return:
        """
        # Gets a new request from redis buffer
        req = self.next_request()
        if req:
            self.crawler.engine.crawl(req, spider=self)
        else:
            # Clear parents
            self.parent_pages = {}

            # Gets a new requests chunk from db
            chunk = models.SiteMap.objects.filter(
                is_processed=False
            ).order_by('depth_level')[0:settings.CHUNK_SIZE]

            if len(chunk) == 0:
                self.logger.debug(debug.bold('No requests found in database. Scheduling crawling'))
                self.recrawl_next()
                return

            # Update processed
            models.SiteMap.objects.filter(id__in=map(lambda x: x.id, chunk)).update(is_processed=True)

            # Schedule requests
            self.logger.debug(debug.success('Scheduling new requests chunk to redis. Size: {}'.format(len(chunk))))
            for page in chunk:
                self.crawler.engine.crawl(
                    Request(
                        url=page.url,
                        dont_filter=True,
                        meta={
                            'item': page
                        },
                        headers={
                            'Referer': page.url
                        },
                    ),
                    spider=self
                )

            # Schedule one-time requests
            items = models.SiteMap.objects.filter(depth_level=-1, is_processed=False).all()
            for i in items:
                self.crawler.engine.crawl(
                    Request(
                        url=i.url,
                        dont_filter=True,
                        meta={
                            'item': i
                        },
                        headers={
                            'Referer': i.url
                        },
                    ),
                    spider=self
                )

    def _get_stored_count(self, agency):
        key = 'agency:stored:%s' % agency.id
        cached_counter = self.server.get(key)

        if cached_counter is None or cached_counter == 0:
            related = self._related(agency)
            requests = models.SiteMap.objects.filter(agency_id__in=related).count()
            cache = models.PageIndex.objects.filter(agency_id__in=related).count()
            cached_counter = requests + cache
            self.server.set(key, cached_counter)

        return int(cached_counter)

    def _add_stored_count(self, agency, stored=0):
        already_stored = self._get_stored_count(agency)
        self.server.set('agency:stored:%s' % agency.id, stored + already_stored)

    def _allowed_extract(self, agency):
        """
        Check if allowed extract links for domain
        :param agency:
        :return:
        """
        return self._get_stored_count(agency) < settings.CACHE_LIMIT

    def _agency(self, ag_id):
        if ag_id in self.agencies_cache.keys():
            return self.agencies_cache[ag_id]

        self.agencies_cache[ag_id] = models.Agency.objects.get(pk=ag_id)
        return self.agencies_cache[ag_id]

    def _related(self, agency):
        if agency.id in self.related_ids.keys():
            return self.related_ids[agency.id]

        self.related_ids[agency.id] = agency.related_ids
        return self.related_ids[agency.id]

    def _allowed_domains(self, agency):
        if agency.id in self.allowed_domains.keys():
            return self.allowed_domains[agency.id]

        self.allowed_domains[agency.id] = agency.allowed_domains
        return self.allowed_domains[agency.id]

    def _insert_sitemap(self, items, agency):
        """
        Creates array of items for sitemap
        :param items:
        :param chunk_size:
        :return:
        """
        created = 0

        if not self._allowed_extract(agency):
            return 0

        # Try to create in one query
        try:
            models.SiteMap.objects.bulk_create(items)
            created += len(items)
        except:
            # Find uniques and create
            existings = models.SiteMap.objects.filter(url__in=[i.url for i in items])
            existings = [a.url for a in existings]
            uniques = filter(lambda x: x.url not in existings, items)

            try:
                models.SiteMap.objects.bulk_create(uniques)
                created += len(uniques)
            except:
                # Create one by one
                for request in uniques:
                    try:
                        request.save()
                        created += 1
                    except:
                        pass

        self._add_stored_count(agency, created)
        return created

    def _insert_cache(self, items):
        """
        Creates array of items for page cache
        :param items:
        :param chunk_size:
        :return:
        """
        created = 0
        # Try to create in one query
        try:
            models.PageIndex.objects.bulk_create(items)
        except:
            # Create one by one
            for page in items:
                try:
                    page.save()
                    created += 1
                except:
                    pass

        return created
